<html>
<head>
	<meta name="layout" content="main"/>
	<title>Sikeres regisztráció</title>
</head>
				<div class="large_window_top">
					<div class="legend">Regisztráció</div>
				</div>
				<div class="large_window_bevel">
					<div class="content">
						<br/><br/>
						<img src="${request.contextPath}/images/vizjel.png" alt="TheTree&trade;" title="TheTree&trade;" align="left"/>
						Köszönjük, hogy minket választottál. Regisztrációd már <b>majdnem kész</b>. Egy megerősítő linket küldtünk a megadott E-Mail címedre.<br/>
						Kattints a linkre, vagy másold be a linket a böngésződ címsorába, és máris aktiválva van az accountod.
						<br/><br/>
						<b>Üdvözlettel: TheTree Tracker staff</b>
						<br/><br/>
						<br/><br/>
						<div class="button" onclick="window.location.href='${g.createLink(controller: 'index')}'">
							<div class="button-left"></div>
							<div class="content">Tovább a belépő felületre</div>
							<div class="button-right"></div>
						</div>
					</div>
				</div>
</html>