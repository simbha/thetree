<html>
	<head>
		<title>Sikeres bug/feature bejelentés</title>
		<meta name="layout" content="main"/>
	</head>
	<body>
		<br/><br/>
		<div class="large_window_top">
			<div class="legend">
				Hiba bejelentése
			</div>
		</div>
		<div class="large_window_bevel">
			<div class="content">
				<img src="images/vizjel.png" alt="TheTree" title="TheTree" align="right" />
				<h3>Köszönjük bejelentésed!</h3>
				Köszönjük, hogy megírtad nekünk javaslatodat/hibajelentésed, és ezáltal hozzájárultál az oldal továbbfejlesztéséhez.<br/>
				<br/>
				Üdvözlettel:<br/>
				<b>TheTree Staff</b>
			</div>
		</div>
	</body>
</html>