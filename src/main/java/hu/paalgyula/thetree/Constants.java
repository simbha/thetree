package hu.paalgyula.thetree;

/**
 * Created by PGYULA on 3/1/14.
 */
public class Constants {
    public final static int TORRENTS_PER_PAGE = 50;
    public static final int FEED_ITEMS_PER_PAGE = 10;
}
